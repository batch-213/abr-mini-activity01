<?php 

$firstName = 'Amiel';
$lastName = 'Oliva';
$age = '18';
$address = 'Manila';

function pesoToDollar($peso) {
	return '$'. round("$peso" / 55 , 2);
}

function determineDepartment($department){
	if($department <= 3 && $department > 0) {
		return 'Department A';
	}
	else if($department <= 6 && $department >= 4) {
		return 'Department B';
	}
	else if($department <= 9 && $department >= 7) {
		return 'Department C';
	}
	else {
		return 'Department does not exist!';
	}
}
