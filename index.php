<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>PHP Activity</title>
</head>
<body>
	<h2>Personal Info</h2>
	<p><?php echo "Full name : $firstName $lastName"; ?></p>
	<p><?php echo "Age : $age"; ?></p>
	<p><?php echo "Adress : $address"; ?></p>

	<h2>Convert Peso to Dollar</h2>
	<p><?php echo pesoToDollar(100); ?></p>

	<h2>Department</h2>
	<p><?php echo determineDepartment(10); ?></p>

</body>
</html>